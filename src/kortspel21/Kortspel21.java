/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kortspel21;

import java.util.*;

/**
 *
 * @author Fredrik
 */
public class Kortspel21 
{
    // Declare global variables
    public static int[] playerHands;
    public static boolean playerTurn;
    public static boolean playerStopped;
    public static Oc_lib oc; // Load my library
    public static Scanner scan;
    
    public static void main(String[] args) 
    {
        
        // Act as constructor
        // Give the global variables their values
        playerHands = new int[2];
        oc = new Oc_lib(); // Create new instance of my lib for useful functions
        scan = new Scanner(System.in);
        playerTurn = true;
        playerStopped = false;
       
        playGame(); // Run Game 
    }
    
    public static void playGame()
    {   
        
        int draw;
        
        oc.write("Hello sir! \nWelcome to Blackjack! \n\nTry and get as close to the number 21 as possible. \nPress ENTER to begin and draw a card, press 'S' then ENTER to stop", true);
        
        // Run the game-loop.
        do  
        {
            // Are we allowed to play?
            if( playerTurn && ! playerStopped && playerHands[1] < 21 ) 
            {
               
                // Scan the keypress
                String key = scan.nextLine();

                // Make sure that the player doesn't want to cancel the draw
                if( key.toLowerCase().equals("s") || key.toLowerCase().equals("n") )
                {
                    // We've chosen to stop, but allow the dealer to continue (Hence Continue otherwise Break)
                    playerStopped = true;
                    continue;
                }
                   
                // Grab a random card
                draw = drawCard();
                
                // Add the cards value to my hand
                playerHands[1] += draw;
                
                // Give the player a message for their current hand
                oc.write("You drew " + draw + ". Your hand is " + playerHands[1], true);
                
                if( playerHands[1] == 21 || playerHands[1] > 21 || ( playerHands[0] >= 16 && playerHands[0] < playerHands[1] ) )
                {
                    // Break the loop as the win / lose conditions have been met
                    break;
                }

            }
            else if(playerHands[0] < 16) // The dealers turn
            {
                try
                {
                    // Act as the dealer is "thinking" 
                    Thread.sleep(1000);
                }
                catch(InterruptedException ex)
                {
                    Thread.currentThread().interrupt();
                }
                
                // Draw a random value 
                draw = drawCard();
                
                // Add the card to the dealers hand
                playerHands[0] += draw;
                oc.write("The dealer drew " + draw + ". The dealers hand is now " + playerHands[0], true);
                
                if( playerHands[0] == 21 || playerHands[0] > 21 || ( playerHands[0] >= 16 && playerHands[1] > playerHands[0] ) )
                {
                    // Win / lose condition? stop the loop.
                    break;
                }
            }
            
            if(playerHands[1] < 21)
            {
                // Switch turn
                playerTurn = !playerTurn;
            }
            
            
        }
        while( (playerHands[1] < 21 && ! playerStopped ) || playerHands[0] < 16  );
        
        oc.write("\n-----------------------------------" ,true);
        oc.write("Your finishing hand is " + playerHands[1] ,true);
        oc.write("The dealers finishing hand is " + playerHands[0] + "\n", true);
        
        // Game is done, check who won the game
        if(playerHands[1] == 21 && playerHands[0] < 21 || playerHands[0] > 21 || ( ( 21-playerHands[1] ) < ( 21-playerHands[0] ) && playerHands[1] <= 21  ) && playerHands[1] <= 21 )
        {
            oc.write("Congratulations, you won! :)", true);
        }
        else
        {
            oc.write("The dealer won this round", true);
        }
        
        oc.write("Wanna play again? 'Y' or 'N'", false);
        String keepPlay = scan.nextLine();
        
        // Player wants to continue, reset variables and run the game once again
        if(keepPlay.toLowerCase().equals("y"))
        {
            playerHands = new int[2];
            playerTurn = true;
            playerStopped = false;
            playGame();
        }
    }
    
    public static int drawCard()
    {
        // Get a random value between 1 and 11
        int rand = oc.random(1, 11);
        
        // Determen if the card 11 is supposed (Ace) to be 11 or 1. 
        if(rand == 11 && ( playerHands[1] + rand ) > 21 || rand == 11 && ( playerHands[1] + rand ) > 16)
        {
            rand = 1;
        }
        // Return the value
        return rand;
    }
    
}
