/*
 * Fredrik Mattiassons "Bra att ha" bibliotek
 * Innehåller funktioner som oftast återanvänds
 * 
 */
package kortspel21;

/**
 *
 * @author Fredrik
 */
public class Oc_lib {
    
    public int random(int min, int max)
    {
       int range = (max - min) + 1;     
       return (int)(Math.random() * range) + min;
    }
    
    public void write(String str, boolean ln)
    {
        if(ln) System.out.println(str);
        else System.out.print(str);
    }
    
}
